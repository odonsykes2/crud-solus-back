package br.com.solus.repositorio;
import java.util.List;

import org.springframework.data.repository.Repository;

import br.com.solus.model.PessoaModelo;
 
public interface PessoaRepositorio extends Repository<PessoaModelo, Integer> {
 
	void save(PessoaModelo pessoa);
 
	void delete(PessoaModelo pessoa);
 
	List<PessoaModelo> findAll();
 
	PessoaModelo findByCodigo(Integer id);
}