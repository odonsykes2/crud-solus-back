package br.com.solus.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
 
import br.com.solus.model.PessoaModelo;
import br.com.solus.model.RespModel;
import br.com.solus.repositorio.PessoaRepositorio;
 
@RestController
@RequestMapping(path= "/service")
public class PessoaService {
 
	@Autowired
	private PessoaRepositorio pessoaRepositorio; 
 

	@RequestMapping(value="/pessoa", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody RespModel salvar(@RequestBody PessoaModelo pessoa){
 
 
		try {
 
			this.pessoaRepositorio.save(pessoa);
 
			return new RespModel(1,"Registro salvo com sucesso!");
 
		}catch(Exception e) {
 
			return new RespModel(0,e.getMessage());			
		}
	}
 
	/**
	 * ATUALIZAR O REGISTRO DE UMA PESSOA
	 * @param pessoa
	 * @return
	 */
	@RequestMapping(value="/pessoa", method = RequestMethod.PUT, produces="application/json")
	public @ResponseBody RespModel atualizar(@RequestBody PessoaModelo pessoa){
 
		try {
 
			this.pessoaRepositorio.save(pessoa);		
 
			return new RespModel(1,"Registro atualizado com sucesso!");
 
		}catch(Exception e) {
 
			return new RespModel(0,e.getMessage());
		}
	}
 
	/**
	 * CONSULTAR TODAS AS PESSOAS
	 * @return
	 */
	@RequestMapping(value="/pessoa", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody List<PessoaModelo> consultar(){
 
		return this.pessoaRepositorio.findAll();
	}
 
	/**
	 * BUSCAR UMA PESSOA PELO CÓDIGO
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value="/pessoa/{codigo}", method = RequestMethod.GET,produces="application/json")
	public @ResponseBody PessoaModelo buscar(@PathVariable("codigo") Integer codigo){
 
		return this.pessoaRepositorio.findByCodigo(codigo);
	}
 
	/***
	 * EXCLUIR UM REGISTRO PELO CÓDIGO
	 * @param codigo
	 * @return
	 */
	@RequestMapping(value="/pessoa/{codigo}", method = RequestMethod.DELETE, produces="application/json")
	public @ResponseBody RespModel excluir(@PathVariable("codigo") Integer codigo){
 
		PessoaModelo pessoaModel = pessoaRepositorio.findByCodigo(codigo);
 
		try {
 
			pessoaRepositorio.delete(pessoaModel);
 
			return new RespModel(1, "Registro excluido com sucesso!");
 
		}catch(Exception e) {
			return new RespModel(0, e.getMessage());
		}
	}
 
}